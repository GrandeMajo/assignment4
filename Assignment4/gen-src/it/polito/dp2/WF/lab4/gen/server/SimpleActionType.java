
package it.polito.dp2.WF.lab4.gen.server;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per simpleActionType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="simpleActionType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://pad.polito.it/WorkflowManagement}actionType">
 *       &lt;sequence>
 *         &lt;element name="nextAction" type="{http://pad.polito.it/WorkflowManagement}nameType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "simpleActionType", propOrder = {
    "nextAction"
})
public class SimpleActionType
    extends ActionType
{

    protected List<String> nextAction;

    /**
     * Gets the value of the nextAction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nextAction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNextAction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getNextAction() {
        if (nextAction == null) {
            nextAction = new ArrayList<String>();
        }
        return this.nextAction;
    }

}
