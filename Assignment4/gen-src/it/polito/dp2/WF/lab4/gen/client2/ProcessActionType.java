
package it.polito.dp2.WF.lab4.gen.client2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per processActionType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="processActionType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://pad.polito.it/WorkflowManagement}actionType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="actionWorkflow" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "processActionType")
public class ProcessActionType
    extends ActionType
{

    @XmlAttribute(name = "actionWorkflow", required = true)
    protected String actionWorkflow;

    /**
     * Recupera il valore della proprietÓ actionWorkflow.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionWorkflow() {
        return actionWorkflow;
    }

    /**
     * Imposta il valore della proprietÓ actionWorkflow.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionWorkflow(String value) {
        this.actionWorkflow = value;
    }

}
