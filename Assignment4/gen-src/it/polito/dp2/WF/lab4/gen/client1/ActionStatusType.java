
package it.polito.dp2.WF.lab4.gen.client1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java per actionStatusType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="actionStatusType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="actor" type="{http://pad.polito.it/WorkflowManagement}actorType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="isTakenInCharge" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="isTerminated" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="terminationTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "actionStatusType", propOrder = {
    "actor"
})
public class ActionStatusType {

    protected ActorType actor;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "isTakenInCharge")
    protected Boolean isTakenInCharge;
    @XmlAttribute(name = "isTerminated")
    protected Boolean isTerminated;
    @XmlAttribute(name = "terminationTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar terminationTime;

    /**
     * Recupera il valore della proprietÓ actor.
     * 
     * @return
     *     possible object is
     *     {@link ActorType }
     *     
     */
    public ActorType getActor() {
        return actor;
    }

    /**
     * Imposta il valore della proprietÓ actor.
     * 
     * @param value
     *     allowed object is
     *     {@link ActorType }
     *     
     */
    public void setActor(ActorType value) {
        this.actor = value;
    }

    /**
     * Recupera il valore della proprietÓ name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Imposta il valore della proprietÓ name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Recupera il valore della proprietÓ isTakenInCharge.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsTakenInCharge() {
        return isTakenInCharge;
    }

    /**
     * Imposta il valore della proprietÓ isTakenInCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTakenInCharge(Boolean value) {
        this.isTakenInCharge = value;
    }

    /**
     * Recupera il valore della proprietÓ isTerminated.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsTerminated() {
        return isTerminated;
    }

    /**
     * Imposta il valore della proprietÓ isTerminated.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTerminated(Boolean value) {
        this.isTerminated = value;
    }

    /**
     * Recupera il valore della proprietÓ terminationTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTerminationTime() {
        return terminationTime;
    }

    /**
     * Imposta il valore della proprietÓ terminationTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTerminationTime(XMLGregorianCalendar value) {
        this.terminationTime = value;
    }

}
