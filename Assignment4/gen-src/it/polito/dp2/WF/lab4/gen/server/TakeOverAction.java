
package it.polito.dp2.WF.lab4.gen.server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per takeOverAction complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="takeOverAction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="actionName" type="{http://pad.polito.it/WorkflowManagement}nameType"/>
 *         &lt;element name="processId" type="{http://pad.polito.it/WorkflowManagement}idType"/>
 *         &lt;element name="actor" type="{http://pad.polito.it/WorkflowManagement}actorType"/>
 *         &lt;element name="clientId" type="{http://pad.polito.it/WorkflowManagement}UUIDType"/>
 *         &lt;element name="requestId" type="{http://pad.polito.it/WorkflowManagement}idType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "takeOverAction", propOrder = {
    "actionName",
    "processId",
    "actor",
    "clientId",
    "requestId"
})
public class TakeOverAction {

    @XmlElement(required = true)
    protected String actionName;
    @XmlElement(required = true)
    protected String processId;
    @XmlElement(required = true)
    protected ActorType actor;
    @XmlElement(required = true)
    protected String clientId;
    @XmlElement(required = true)
    protected String requestId;

    /**
     * Recupera il valore della proprietÓ actionName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * Imposta il valore della proprietÓ actionName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionName(String value) {
        this.actionName = value;
    }

    /**
     * Recupera il valore della proprietÓ processId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessId() {
        return processId;
    }

    /**
     * Imposta il valore della proprietÓ processId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessId(String value) {
        this.processId = value;
    }

    /**
     * Recupera il valore della proprietÓ actor.
     * 
     * @return
     *     possible object is
     *     {@link ActorType }
     *     
     */
    public ActorType getActor() {
        return actor;
    }

    /**
     * Imposta il valore della proprietÓ actor.
     * 
     * @param value
     *     allowed object is
     *     {@link ActorType }
     *     
     */
    public void setActor(ActorType value) {
        this.actor = value;
    }

    /**
     * Recupera il valore della proprietÓ clientId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Imposta il valore della proprietÓ clientId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Recupera il valore della proprietÓ requestId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Imposta il valore della proprietÓ requestId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

}
