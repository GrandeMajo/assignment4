
package it.polito.dp2.WF.lab4.gen.server;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.polito.dp2.WF.lab4.gen.server package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InvalidRequestError_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "invalidRequestError");
    private final static QName _GetWorkflowNamesResponse_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "getWorkflowNamesResponse");
    private final static QName _TakeOverAction_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "takeOverAction");
    private final static QName _UnknownNamesError_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "unknownNamesError");
    private final static QName _GetLastModifiedTimeResponse_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "getLastModifiedTimeResponse");
    private final static QName _GetWorkflows_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "getWorkflows");
    private final static QName _GetClientIdResponse_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "getClientIdResponse");
    private final static QName _CompleteActionResponse_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "completeActionResponse");
    private final static QName _CompleteAction_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "completeAction");
    private final static QName _TakeOverActionResponse_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "takeOverActionResponse");
    private final static QName _CreateProcess_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "createProcess");
    private final static QName _GetClientId_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "getClientId");
    private final static QName _GetProcessesResponse_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "getProcessesResponse");
    private final static QName _SystemError_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "systemError");
    private final static QName _CreateProcessResponse_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "createProcessResponse");
    private final static QName _UnknownIdentifiersError_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "unknownIdentifiersError");
    private final static QName _GetWorkflowNames_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "getWorkflowNames");
    private final static QName _GetProcesses_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "getProcesses");
    private final static QName _GetWorkflowsResponse_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "getWorkflowsResponse");
    private final static QName _GetLastModifiedTime_QNAME = new QName("http://pad.polito.it/WorkflowManagement", "getLastModifiedTime");
    private final static QName _GetWorkflowsPageNumber_QNAME = new QName("", "pageNumber");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.polito.dp2.WF.lab4.gen.server
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetClientId }
     * 
     */
    public GetClientId createGetClientId() {
        return new GetClientId();
    }

    /**
     * Create an instance of {@link TakeOverActionResponse }
     * 
     */
    public TakeOverActionResponse createTakeOverActionResponse() {
        return new TakeOverActionResponse();
    }

    /**
     * Create an instance of {@link CreateProcess }
     * 
     */
    public CreateProcess createCreateProcess() {
        return new CreateProcess();
    }

    /**
     * Create an instance of {@link CreateProcessResponse }
     * 
     */
    public CreateProcessResponse createCreateProcessResponse() {
        return new CreateProcessResponse();
    }

    /**
     * Create an instance of {@link UnknownIdentifiersError }
     * 
     */
    public UnknownIdentifiersError createUnknownIdentifiersError() {
        return new UnknownIdentifiersError();
    }

    /**
     * Create an instance of {@link SystemError }
     * 
     */
    public SystemError createSystemError() {
        return new SystemError();
    }

    /**
     * Create an instance of {@link GetWorkflowsResponse }
     * 
     */
    public GetWorkflowsResponse createGetWorkflowsResponse() {
        return new GetWorkflowsResponse();
    }

    /**
     * Create an instance of {@link GetLastModifiedTime }
     * 
     */
    public GetLastModifiedTime createGetLastModifiedTime() {
        return new GetLastModifiedTime();
    }

    /**
     * Create an instance of {@link GetWorkflowNames }
     * 
     */
    public GetWorkflowNames createGetWorkflowNames() {
        return new GetWorkflowNames();
    }

    /**
     * Create an instance of {@link GetProcesses }
     * 
     */
    public GetProcesses createGetProcesses() {
        return new GetProcesses();
    }

    /**
     * Create an instance of {@link GetProcessesResponse }
     * 
     */
    public GetProcessesResponse createGetProcessesResponse() {
        return new GetProcessesResponse();
    }

    /**
     * Create an instance of {@link GetLastModifiedTimeResponse }
     * 
     */
    public GetLastModifiedTimeResponse createGetLastModifiedTimeResponse() {
        return new GetLastModifiedTimeResponse();
    }

    /**
     * Create an instance of {@link GetWorkflows }
     * 
     */
    public GetWorkflows createGetWorkflows() {
        return new GetWorkflows();
    }

    /**
     * Create an instance of {@link GetClientIdResponse }
     * 
     */
    public GetClientIdResponse createGetClientIdResponse() {
        return new GetClientIdResponse();
    }

    /**
     * Create an instance of {@link InvalidRequestError }
     * 
     */
    public InvalidRequestError createInvalidRequestError() {
        return new InvalidRequestError();
    }

    /**
     * Create an instance of {@link GetWorkflowNamesResponse }
     * 
     */
    public GetWorkflowNamesResponse createGetWorkflowNamesResponse() {
        return new GetWorkflowNamesResponse();
    }

    /**
     * Create an instance of {@link TakeOverAction }
     * 
     */
    public TakeOverAction createTakeOverAction() {
        return new TakeOverAction();
    }

    /**
     * Create an instance of {@link UnknownNamesError }
     * 
     */
    public UnknownNamesError createUnknownNamesError() {
        return new UnknownNamesError();
    }

    /**
     * Create an instance of {@link CompleteAction }
     * 
     */
    public CompleteAction createCompleteAction() {
        return new CompleteAction();
    }

    /**
     * Create an instance of {@link CompleteActionResponse }
     * 
     */
    public CompleteActionResponse createCompleteActionResponse() {
        return new CompleteActionResponse();
    }

    /**
     * Create an instance of {@link ProcessActionType }
     * 
     */
    public ProcessActionType createProcessActionType() {
        return new ProcessActionType();
    }

    /**
     * Create an instance of {@link SimpleActionType }
     * 
     */
    public SimpleActionType createSimpleActionType() {
        return new SimpleActionType();
    }

    /**
     * Create an instance of {@link WorkflowType }
     * 
     */
    public WorkflowType createWorkflowType() {
        return new WorkflowType();
    }

    /**
     * Create an instance of {@link ProcessType }
     * 
     */
    public ProcessType createProcessType() {
        return new ProcessType();
    }

    /**
     * Create an instance of {@link ErrorInfoType }
     * 
     */
    public ErrorInfoType createErrorInfoType() {
        return new ErrorInfoType();
    }

    /**
     * Create an instance of {@link ActionStatusType }
     * 
     */
    public ActionStatusType createActionStatusType() {
        return new ActionStatusType();
    }

    /**
     * Create an instance of {@link ActorType }
     * 
     */
    public ActorType createActorType() {
        return new ActorType();
    }

    /**
     * Create an instance of {@link ActionType }
     * 
     */
    public ActionType createActionType() {
        return new ActionType();
    }

    /**
     * Create an instance of {@link WorkflowMonitorType }
     * 
     */
    public WorkflowMonitorType createWorkflowMonitorType() {
        return new WorkflowMonitorType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidRequestError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "invalidRequestError")
    public JAXBElement<InvalidRequestError> createInvalidRequestError(InvalidRequestError value) {
        return new JAXBElement<InvalidRequestError>(_InvalidRequestError_QNAME, InvalidRequestError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWorkflowNamesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "getWorkflowNamesResponse")
    public JAXBElement<GetWorkflowNamesResponse> createGetWorkflowNamesResponse(GetWorkflowNamesResponse value) {
        return new JAXBElement<GetWorkflowNamesResponse>(_GetWorkflowNamesResponse_QNAME, GetWorkflowNamesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TakeOverAction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "takeOverAction")
    public JAXBElement<TakeOverAction> createTakeOverAction(TakeOverAction value) {
        return new JAXBElement<TakeOverAction>(_TakeOverAction_QNAME, TakeOverAction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnknownNamesError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "unknownNamesError")
    public JAXBElement<UnknownNamesError> createUnknownNamesError(UnknownNamesError value) {
        return new JAXBElement<UnknownNamesError>(_UnknownNamesError_QNAME, UnknownNamesError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLastModifiedTimeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "getLastModifiedTimeResponse")
    public JAXBElement<GetLastModifiedTimeResponse> createGetLastModifiedTimeResponse(GetLastModifiedTimeResponse value) {
        return new JAXBElement<GetLastModifiedTimeResponse>(_GetLastModifiedTimeResponse_QNAME, GetLastModifiedTimeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWorkflows }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "getWorkflows")
    public JAXBElement<GetWorkflows> createGetWorkflows(GetWorkflows value) {
        return new JAXBElement<GetWorkflows>(_GetWorkflows_QNAME, GetWorkflows.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClientIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "getClientIdResponse")
    public JAXBElement<GetClientIdResponse> createGetClientIdResponse(GetClientIdResponse value) {
        return new JAXBElement<GetClientIdResponse>(_GetClientIdResponse_QNAME, GetClientIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompleteActionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "completeActionResponse")
    public JAXBElement<CompleteActionResponse> createCompleteActionResponse(CompleteActionResponse value) {
        return new JAXBElement<CompleteActionResponse>(_CompleteActionResponse_QNAME, CompleteActionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompleteAction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "completeAction")
    public JAXBElement<CompleteAction> createCompleteAction(CompleteAction value) {
        return new JAXBElement<CompleteAction>(_CompleteAction_QNAME, CompleteAction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TakeOverActionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "takeOverActionResponse")
    public JAXBElement<TakeOverActionResponse> createTakeOverActionResponse(TakeOverActionResponse value) {
        return new JAXBElement<TakeOverActionResponse>(_TakeOverActionResponse_QNAME, TakeOverActionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProcess }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "createProcess")
    public JAXBElement<CreateProcess> createCreateProcess(CreateProcess value) {
        return new JAXBElement<CreateProcess>(_CreateProcess_QNAME, CreateProcess.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClientId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "getClientId")
    public JAXBElement<GetClientId> createGetClientId(GetClientId value) {
        return new JAXBElement<GetClientId>(_GetClientId_QNAME, GetClientId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "getProcessesResponse")
    public JAXBElement<GetProcessesResponse> createGetProcessesResponse(GetProcessesResponse value) {
        return new JAXBElement<GetProcessesResponse>(_GetProcessesResponse_QNAME, GetProcessesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SystemError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "systemError")
    public JAXBElement<SystemError> createSystemError(SystemError value) {
        return new JAXBElement<SystemError>(_SystemError_QNAME, SystemError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProcessResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "createProcessResponse")
    public JAXBElement<CreateProcessResponse> createCreateProcessResponse(CreateProcessResponse value) {
        return new JAXBElement<CreateProcessResponse>(_CreateProcessResponse_QNAME, CreateProcessResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnknownIdentifiersError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "unknownIdentifiersError")
    public JAXBElement<UnknownIdentifiersError> createUnknownIdentifiersError(UnknownIdentifiersError value) {
        return new JAXBElement<UnknownIdentifiersError>(_UnknownIdentifiersError_QNAME, UnknownIdentifiersError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWorkflowNames }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "getWorkflowNames")
    public JAXBElement<GetWorkflowNames> createGetWorkflowNames(GetWorkflowNames value) {
        return new JAXBElement<GetWorkflowNames>(_GetWorkflowNames_QNAME, GetWorkflowNames.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcesses }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "getProcesses")
    public JAXBElement<GetProcesses> createGetProcesses(GetProcesses value) {
        return new JAXBElement<GetProcesses>(_GetProcesses_QNAME, GetProcesses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWorkflowsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "getWorkflowsResponse")
    public JAXBElement<GetWorkflowsResponse> createGetWorkflowsResponse(GetWorkflowsResponse value) {
        return new JAXBElement<GetWorkflowsResponse>(_GetWorkflowsResponse_QNAME, GetWorkflowsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLastModifiedTime }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pad.polito.it/WorkflowManagement", name = "getLastModifiedTime")
    public JAXBElement<GetLastModifiedTime> createGetLastModifiedTime(GetLastModifiedTime value) {
        return new JAXBElement<GetLastModifiedTime>(_GetLastModifiedTime_QNAME, GetLastModifiedTime.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pageNumber", scope = GetWorkflows.class)
    public JAXBElement<BigInteger> createGetWorkflowsPageNumber(BigInteger value) {
        return new JAXBElement<BigInteger>(_GetWorkflowsPageNumber_QNAME, BigInteger.class, GetWorkflows.class, value);
    }

}
