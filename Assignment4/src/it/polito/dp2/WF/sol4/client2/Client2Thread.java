package it.polito.dp2.WF.sol4.client2;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;

import it.polito.dp2.WF.WorkflowMonitorException;
import it.polito.dp2.WF.lab4.gen.client1.WorkflowInfo;
import it.polito.dp2.WF.lab4.gen.client1.WorkflowInfoImplementationService;
import it.polito.dp2.WF.lab4.gen.client2.ActionStatusType;
import it.polito.dp2.WF.lab4.gen.client2.InvalidRequestErrorException;
import it.polito.dp2.WF.lab4.gen.client2.ProcessCreator;
import it.polito.dp2.WF.lab4.gen.client2.ProcessCreatorImplementationService;
import it.polito.dp2.WF.lab4.gen.client2.ProcessType;
import it.polito.dp2.WF.lab4.gen.client2.SystemErrorException;


public class Client2Thread extends Thread {

	private static final Logger logger = Logger.getLogger(Client2Thread.class.getName());

	private int times;
	private int sleep;
	private String url;
	private Random random;
	private String clientId;
	private HashSet<String> requestIds; // to avoid repeated requestId 


	public Client2Thread(String url, int times, int sleep) throws WorkflowMonitorException {
		this.url = url;
		this.times = times;
		this.sleep = sleep;
		requestIds = new HashSet<String>();
		random = new Random();
	}

	@Override
	public void run() {
		super.run();
		try {
			for (int i = 0; i < times; i++) {
				service();
				Thread.sleep(random.nextInt(sleep));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void service() throws MalformedURLException, SystemErrorException, it.polito.dp2.WF.lab4.gen.client1.SystemErrorException, InvalidRequestErrorException {
		logger.info(">>>>>>>>>>> Starting Client 2 tester... <<<<<<<<<<<");
		ProcessCreatorImplementationService processService;
		processService = (url == null) ?
				new ProcessCreatorImplementationService() :
					new ProcessCreatorImplementationService(new URL(url));				

		ProcessCreator processProxy = processService.getProcessCreator();

		if (clientId == null)
			clientId = processProxy.getClientId();

		logger.info("Client ID: " + clientId);

		WorkflowInfoImplementationService infoService = new WorkflowInfoImplementationService(new URL("http://localhost:7071/wfinfo"));	
		WorkflowInfo infoProxy = infoService.getWorkflowInfo();
		Holder<XMLGregorianCalendar> lastModifiedTime = new Holder<XMLGregorianCalendar>();
		Holder<List<String>> workflowNames = new Holder<List<String>>();
		infoProxy.getWorkflowNames(lastModifiedTime, workflowNames);
		String workflowName = workflowNames.value.get(new Random().nextInt(workflowNames.value.size()-1));	
		logger.info("Choosen workflow: " + workflowName);

		logger.info("Sending request to service...");

		ProcessType process = processProxy.createProcess(workflowName, clientId, getRequestId());

		logger.info("Got process with ID: " + process.getId());

		printProcessInfos(process);			

		logger.info(">>>>>>>>>>> Client 2 tester terminated <<<<<<<<<<<");		
	}

	private String getRequestId() {
		String requestId;
		do {
			requestId = RandomStringGenerator.getRandomString(Resources.REQUEST_ID_LENGTH);
		} while (requestIds.contains(requestId));

		requestIds.add(requestId);

		return requestId;
	}

	public void printProcessInfos(ProcessType process) {
		StringBuilder sb = new StringBuilder("Process created:")
				.append("\n- ID: ").append(process.getId())
				.append("\n- workflow: ").append(process.getWorkflow())
				.append("\n- startTime: ").append(process.getStartTime())
				.append("\n- actionStatus: ");

		int i = 1;
		for (ActionStatusType action : process.getActionStatus())
			sb.append("\n\t").append(i++).append(". ").append(action.getName());

		logger.info(sb.toString());
	}
}
