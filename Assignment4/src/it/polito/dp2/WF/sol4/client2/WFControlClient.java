package it.polito.dp2.WF.sol4.client2;

import java.net.URL;
import java.util.HashSet;
import java.util.Random;
import java.util.logging.Logger;

import it.polito.dp2.WF.lab4.gen.client2.ActionStatusType;
import it.polito.dp2.WF.lab4.gen.client2.InvalidRequestErrorException;
import it.polito.dp2.WF.lab4.gen.client2.ProcessCreator;
import it.polito.dp2.WF.lab4.gen.client2.ProcessCreatorImplementationService;
import it.polito.dp2.WF.lab4.gen.client2.ProcessType;
import it.polito.dp2.WF.lab4.gen.client2.SystemErrorException;

public class WFControlClient {
	private static final int SUCCESS 		= 0;
	private static final int FAILURE 		= 1;
	private static final int SERVICE_ERROR 	= 2;
	
	private static final int THREADS_NUMBER	= 10;	
	private static final int TIMES_BOUND	= 200;
	private static final int SLEEP_BOUND	= 2000;	// milliseconds
//	private static final int BINDING_PORT 	= 8888;
	
	private static final Logger logger = Logger.getLogger(WFControlClient.class.getName());
	
	private String url;
	private String clientId;
	private HashSet<String> requestIds; // to avoid repeated requestId 

	public WFControlClient(String url)  {
		this.url = url;
		requestIds = new HashSet<String>();
		Resources.setLogHandler(logger, false);
	}
	
	public void service(String workflowName) {
		logger.info("Starting Client 2...");
		ProcessCreatorImplementationService processService;
		try {
			processService = (url == null) ?
					new ProcessCreatorImplementationService() :
						new ProcessCreatorImplementationService(new URL(url));				

			ProcessCreator processProxy = processService.getProcessCreator();
			
			if (clientId == null)
				clientId = processProxy.getClientId();
			
			logger.info("Client ID: " + clientId);
			
//			BindingProvider provider = (BindingProvider) processProxy;
//			provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://localhost:" + BINDING_PORT + "/wfcontrol");
			
			// ====== AGGIUNTA DA TOGLIERE! =======================
			// sceglie un workflow a caso a cui aggiungere un processo
//			WorkflowInfoImplementationService infoService = new WorkflowInfoImplementationService(new URL("http://localhost:7071/wfinfo"));	
//			WorkflowInfo infoProxy = infoService.getWorkflowInfo();
//			Holder<XMLGregorianCalendar> lastModifiedTime = new Holder<XMLGregorianCalendar>();
//			Holder<List<String>> workflowNames = new Holder<List<String>>();
//			infoProxy.getWorkflowNames(lastModifiedTime, workflowNames);
//			workflowName = workflowNames.value.get(new Random().nextInt(workflowNames.value.size()-1));	
//			logger.info("Choosen workflow: " + workflowName);
			// ====================================================

			logger.info("Sending request to service...");

			ProcessType process = processProxy.createProcess(workflowName, clientId, getRequestId());
			
			logger.info("Got process with ID: " + process.getId());
			
			printProcessInfos(process);			
			
		} catch (InvalidRequestErrorException iree) {
			logger.severe(iree.getMessage());
			System.exit(FAILURE);
		} catch (SystemErrorException see) {
			logger.severe(see.getMessage());
			System.exit(SERVICE_ERROR);
		} catch (Exception e) {
			logger.severe(e.getMessage());
			System.exit(SERVICE_ERROR);
		}
		
		logger.info("Client 2 correctly terminated");
		System.exit(SUCCESS);
	}
	
	private String getRequestId() {
		String requestId;
		do {
			requestId = RandomStringGenerator.getRandomString(Resources.REQUEST_ID_LENGTH);
		} while (requestIds.contains(requestId));
		
		requestIds.add(requestId);
		
		return requestId;
	}
	
	public void printProcessInfos(ProcessType process) {
		StringBuilder sb = new StringBuilder("Process created:")
				.append("\n- ID: ").append(process.getId())
				.append("\n- workflow: ").append(process.getWorkflow())
				.append("\n- startTime: ").append(process.getStartTime())
				.append("\n- actionStatus: ");
		
		int i = 1;
		for (ActionStatusType action : process.getActionStatus())
			sb.append("\n\t").append(i++).append(". ").append(action.getName());
		
		logger.info(sb.toString());
	}
	
	public static long nextLong(Random random, long bound) {
		// error checking and 2^x checking removed for simplicity.
		long bits, val;
		do {
			bits = (random.nextLong() << 1) >>> 1;
			val = bits % bound;
		} while (bits - val + (bound - 1) < 0L);
		return val;
	}
	
	/**
	 * Ricorda: quando usi questo metodo per testare commenta 
	 * l'istruzione {@code System.exit(SUCCESS)} nel metodo {@code #service(String)}
	 * 
	 * @param url
	 * @param workflowName
	 */
	@SuppressWarnings("unused")
	private static void multiclientTester(final String url, final String workflowName) {
		for(int t = 0; t < THREADS_NUMBER; t++) {
			new Thread(new Runnable() {
				public void run() {
					try {
						WFControlClient client = new WFControlClient(url);
						Random random = new Random();
						int times = random.nextInt(TIMES_BOUND) + 50;
						
						for (int i = 0; i < times; i++) {				
							client.service(workflowName);
							Thread.sleep(random.nextInt(SLEEP_BOUND));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}).start();
		}
	}
	
	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("Error: wrong number of parameter, expected 2 parameters [ <URL> , <workflow name> ]");
			System.exit(FAILURE);
		}
		
		if (args[1] == null) {
			System.err.println("Error: null workflow name");
			System.exit(FAILURE);
		}
		
		try {
//			multiclientTester(args[0], args[1]);
			WFControlClient client = new WFControlClient(args[0]);
			client.service(args[1]);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(FAILURE);
		}
	}

//	public static void main(String[] args) {
//		if (args.length != 2) {
//			System.err.println("Error: wrong number of parameter, expected 2 parameters [ <URL> , <workflow name> ]");
//			System.exit(FAILURE);
//		}
//		
//		if (args[1] == null) {
//			System.err.println("Error: null workflow name");
//			System.exit(FAILURE);
//		}
//		
//		final String url = args[0];
//		final String workflowName = args[1];
//		
//		for(int t = 0; t < THREADS_NUMBER; t++) {
//			new Thread(new Runnable() {
//				
//				public void run() {
//					try {
//						WFControlClient client = new WFControlClient(url);
//						Random random = new Random();
//						int times = random.nextInt(TIMES_BOUND) + 50;
//						
//						for (int i = 0; i < times; i++) {				
//							client.service(workflowName);
//							Thread.sleep(nextLong(random, SLEEP_BOUND));
//						}
//						
//					} catch (Exception e) {
//						System.err.println(e.getMessage());
//						System.exit(FAILURE);
//					}
//				}
//			}).start();
//		}
//	}
}
