package it.polito.dp2.WF.sol4.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

import it.polito.dp2.WF.ActionReader;
import it.polito.dp2.WF.ActionStatusReader;
import it.polito.dp2.WF.Actor;
import it.polito.dp2.WF.ProcessActionReader;
import it.polito.dp2.WF.ProcessReader;
import it.polito.dp2.WF.SimpleActionReader;
import it.polito.dp2.WF.WorkflowMonitor;
import it.polito.dp2.WF.WorkflowMonitorException;
import it.polito.dp2.WF.WorkflowMonitorFactory;
import it.polito.dp2.WF.WorkflowReader;
import it.polito.dp2.WF.lab4.gen.server.ActionStatusType;
import it.polito.dp2.WF.lab4.gen.server.ActionType;
import it.polito.dp2.WF.lab4.gen.server.ActorType;
import it.polito.dp2.WF.lab4.gen.server.InvalidRequestError;
import it.polito.dp2.WF.lab4.gen.server.InvalidRequestError_Exception;
import it.polito.dp2.WF.lab4.gen.server.ProcessActionType;
import it.polito.dp2.WF.lab4.gen.server.ProcessType;
import it.polito.dp2.WF.lab4.gen.server.SimpleActionType;
import it.polito.dp2.WF.lab4.gen.server.UnknownIdentifiersError;
import it.polito.dp2.WF.lab4.gen.server.UnknownIdentifiersError_Exception;
import it.polito.dp2.WF.lab4.gen.server.UnknownNamesError;
import it.polito.dp2.WF.lab4.gen.server.UnknownNamesError_Exception;
import it.polito.dp2.WF.lab4.gen.server.WorkflowType;

/**
 * Class that manages workflow and process related data.
 * Implemented using {@code Singleton} pattern.
 * 
 * @author Gianluca Maiorino
 *
 */
public class WorkflowManager {
	
	private static Logger logger = Logger.getLogger(WorkflowManager.class.getName());
	
	private static final int PAGE_SIZE = 10;
	
	/*
	 * A map that contains all the workflows
	 */
	private volatile ConcurrentHashMap<String, WorkflowType> workflows;
	
	/*
	 * A map that contains all the processes, the key is generated at process creation time
	 */
	private volatile ConcurrentHashMap<String, ProcessType> processes;
	
	private volatile AtomicLong lastModifiedTime;
	private WorkflowMonitor monitor;
	
	private WorkflowManager() throws WorkflowMonitorException, SecurityException, IOException {
		Resources.setLogHandler(logger, false);
		WorkflowMonitorFactory factory = WorkflowMonitorFactory.newInstance();
		monitor = factory.newWorkflowMonitor();
		lastModifiedTime 	= new AtomicLong(0);
		workflows 			= new ConcurrentHashMap<String, WorkflowType>();
		processes 			= new ConcurrentHashMap<String, ProcessType>();
		init();
		logger.fine(WorkflowManager.class.getName() + " correctly created");
	}
	
	public static class WorkflowManagerHolder {
		private static final WorkflowManager MANAGER;
		
		static {
			WorkflowManager temp = null;
			try {
				temp = new WorkflowManager();
			} catch (Exception e) {
				temp = null;
				logger.severe(e.getMessage());
			}
			MANAGER = temp;
		}
	}
	
	public static WorkflowManager getInstance() {
		return WorkflowManagerHolder.MANAGER;
	}
	
	public Set<String> getWorkflowNames() {
		logger.info("Required workflow names");
		return workflows.keySet();
	}
	
	public ResultHolder<WorkflowType> getWorkflows(Set<String> workflowNames, int pageNumber) throws UnknownNamesError_Exception {
		logger.entering(WorkflowManagerHolder.class.getName(), "getWorkflows");
		List<WorkflowType> results = new LinkedList<WorkflowType>();
		
		if (workflowNames == null || workflowNames.isEmpty()) {
			results.addAll(workflows.values());
		} else {
			UnknownNamesError unknownNames = ErrorUtils.getUnknownNamesError();
			boolean throwException = false;
			
			for (String name : workflowNames) {
				WorkflowType workflow =	workflows.get(name);
				if (workflow == null) {
					throwException = true;
					unknownNames.getNames().add(name);
				} else if (!throwException) {
					results.add(workflow);
				}
			}
			
			if (throwException) {
				logger.severe("Unknown workflow names received: " + unknownNames.getNames());
				throw new UnknownNamesError_Exception(ErrorUtils.UNKNOWN_NAMES, unknownNames);
			}
		}
		
		// paginazione
		int nPages = 1;
		int size = results.size();
		if (size > PAGE_SIZE) {
			int indexes[] = getIndexes(size, pageNumber);
			results = results.subList(indexes[0], indexes[1]);
			nPages = indexes[2];
			logger.info("Required page " + pageNumber + " of " + nPages);
		}
		
		logger.exiting(WorkflowManagerHolder.class.getName(), "getWorkflows");
		return new ResultHolder<WorkflowType>(results, nPages);
	}
	
	public ResultHolder<ProcessType> getProcesses(Set<String> processIds, Set<String> workflowNames, int pageNumber) throws UnknownIdentifiersError_Exception, UnknownNamesError_Exception {
		logger.entering(WorkflowManagerHolder.class.getName(), "getProcesses");
		List<ProcessType> results = new LinkedList<ProcessType>();
		boolean throwException = false;
		
		if (processIds != null && !processIds.isEmpty()) {
			logger.info("Process IDs received (" + processIds.size() + "): " + processIds);
			UnknownIdentifiersError unknownIdentifiers = ErrorUtils.getUnknownIdentifiersError();
			
			for (String processId : processIds) {
				ProcessType process = processes.get(processId);
				if (process == null) {
					throwException = true;
					unknownIdentifiers.getIds().add(processId);
				} else if (!throwException) {
					results.add(process);
				}
			}
			
			if (throwException) {
				logger.severe("Unknown process IDs received: " + unknownIdentifiers.getIds());
				throw new UnknownIdentifiersError_Exception(ErrorUtils.UNKNOWN_NAMES, unknownIdentifiers);
			}
			
		} else if (workflowNames != null && !workflowNames.isEmpty()) {
			logger.info("Workflow names received (" + workflowNames.size() + "): " + workflowNames);
			UnknownNamesError unknownNames = ErrorUtils.getUnknownNamesError();
			
			for (String name : workflowNames) {
				WorkflowType workflow =	workflows.get(name);
				if (workflow == null) {
					throwException = true;
					unknownNames.getNames().add(name);
				} else if (!throwException) {
					List<String> processesIds = workflow.getProcesses();
					for (String processId : processesIds) {
						results.add(processes.get(processId));
					}
				}
			}
			
			if (throwException) {
				logger.severe("Unknown workflow names received: " + unknownNames.getNames());
				throw new UnknownNamesError_Exception(ErrorUtils.UNKNOWN_NAMES, unknownNames);
			}
			
		} else {
			logger.info("Nor process IDs neither workflow names sepcified");
			results.addAll(processes.values());
		}
		
		int nPages = 1;
		int size = results.size();
		if (size > PAGE_SIZE) {
			int indexes[] = getIndexes(size, pageNumber);
			results = results.subList(indexes[0], indexes[1] + 1);
			nPages = indexes[2];
			logger.info("Required page " + pageNumber + " of " + nPages);
		}
		
		logger.exiting(WorkflowManagerHolder.class.getName(), "getProcesses");
		return new ResultHolder<ProcessType>(results, nPages);
	}
	
	public ProcessType createProcess(String workflowName) throws InvalidRequestError_Exception {
		logger.entering(WorkflowManagerHolder.class.getName(), "createProcesses");
		
		WorkflowType workflow = workflows.get(workflowName);
		if (workflow == null) {
			logger.severe("Unknown workflow names received: " + workflowName);
			InvalidRequestError invalidRequestError = ErrorUtils.getInvalidRequestError(ErrorUtils.UNKNOWN_NAME);
			throw new InvalidRequestError_Exception(ErrorUtils.UNKNOWN_NAME, invalidRequestError);
		}
		
		List<ActionStatusType> actionStatusTypes = new ArrayList<ActionStatusType>();
		List<SimpleActionType> simpleActionTypes = workflow.getSimpleAction();
		List<ProcessActionType> processActionTypes = workflow.getProcessAction();
		
		for (SimpleActionType simpleActionType : simpleActionTypes) {
			if (simpleActionType.isIsAutomaticallyInstantiated()) {
				ActionStatusType actionStatusType = new ActionStatusType();
				actionStatusType.setName(simpleActionType.getName());	
				actionStatusTypes.add(actionStatusType);
			}
		}
		
		for (ProcessActionType processActionType : processActionTypes) {
			if (processActionType.isIsAutomaticallyInstantiated()) {
				ActionStatusType actionStatusType = new ActionStatusType();
				actionStatusType.setName(processActionType.getName());
				actionStatusTypes.add(actionStatusType);
			}
		}
		
		ProcessType process = new ProcessType();
		process.setWorkflow(workflowName);
		process.setStartTime(Resources.getXMLGregorianCalendar());
		process.getActionStatus().addAll(actionStatusTypes);
		addProcess(process, workflow);
		
		logger.exiting(WorkflowManagerHolder.class.getName(), "createProcesses");
		return process;
	}

	/**
	 * Metodo per aggiungere un nuovo processo, in esso vengono acquisiti 2 lock: uno su {@link #processes} 
	 * e uno su {@link #workflows} in modo da aggiornare le due strutture dati contemporaneamente per 
	 * evitare letture sporche.
	 * 
	 * @param process the new process created
	 * @param workflow the workflow of the new process
	 * @return boolean true if the new process is correctly added, false otherwise
	 */
	private boolean addProcess(ProcessType process, WorkflowType workflow) {
		synchronized (processes) {
			synchronized (workflows) {
				String processId = getRandomString();
				process.setId(processId);
				processes.put(processId, process);
				logger.info("Added process: " + processId);
				boolean result = workflow.getProcesses().add(processId);
				logger.info("Added process " + processId + " to workflow" + workflow.getName());
				lastModifiedTime.set(System.currentTimeMillis());
				return result;
			}
		}
	}
	
	/**
	 * Questo metodo è chiamato solo all'interno del metodo {@link #addProcess(ProcessType)} all'interno di un blocco synchronized.
	 * Pertanto può essere chiamato solo da un thread alla volta.
	 */
	private String getRandomString() {
		String randomString;
		// do while loop to avoid birthday paradox
		do {			
			randomString = RandomStringGenerator.getRandomString(Resources.PROCESS_ID_LENGTH);
		} while (processes.containsKey(randomString));
		return randomString;
	}
	
	private int[] getIndexes(int size, int page) {
		int nPages = size / PAGE_SIZE;
		
		if (size % PAGE_SIZE > 0)
			nPages++;
		
		if (page <= 0 || page > nPages)
			page = 1;
		
		int startIndex = (page - 1) * PAGE_SIZE;
		int endIndex = startIndex + PAGE_SIZE - 1;
		
		if (endIndex > size - 1)
			endIndex = size - 1;
		
		return new int[] { startIndex, endIndex, nPages };
	}
	
	public Calendar getLastModifiedTime() {
		long lmt = lastModifiedTime.get();
		
		if (lmt == 0)
			return null;
			
		Calendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(lmt);
		return calendar;
	}
	
	// ====================================================================//
	/*
	 * Methods to get workflows and processes from WorkflowMonitor 
	 */

	private synchronized void  init() {
		Set<WorkflowReader> workflowReaders = monitor.getWorkflows();
		for (WorkflowReader workflowReader : workflowReaders)
			workflows.put(workflowReader.getName(), getWorkflowType(workflowReader));
		
		Set<ProcessReader> processReaders = monitor.getProcesses();
		for (ProcessReader processReader : processReaders) {
			ProcessType processType = getProcessType(processReader);
			processes.put(processType.getId(), processType);
		}
		
		lastModifiedTime.set(System.currentTimeMillis());
	}
	
	private WorkflowType getWorkflowType(WorkflowReader workflowReader) {
		WorkflowType workflow = new WorkflowType();
		workflow.setName(workflowReader.getName());
		
		List<SimpleActionType> simpleActionTypes = workflow.getSimpleAction();
		List<ProcessActionType> processActionTypes = workflow.getProcessAction();
		Set<ActionReader> actionReaders = workflowReader.getActions();
		
		for (ActionReader actionReader : actionReaders) {
			if (actionReader instanceof SimpleActionReader)
				simpleActionTypes.add(getSimpleActionType((SimpleActionReader) actionReader));
			else
				processActionTypes.add(getProcessActionType((ProcessActionReader) actionReader));
		}
		
		return workflow;
	}
	
	@SuppressWarnings("unused")
	private ActionType getActionType(ActionReader actionReader) {
		ActionType action = new ActionType();
		action.setName(actionReader.getName());
		action.setEnclosingWorkflow(actionReader.getEnclosingWorkflow().getName());
		action.setRole(actionReader.getRole());
		action.setIsAutomaticallyInstantiated(actionReader.isAutomaticallyInstantiated());
		
		return action; 
	}
	
	private SimpleActionType getSimpleActionType(SimpleActionReader simpleActionReader) {
		SimpleActionType simpleAction = new SimpleActionType();
		simpleAction.setName(simpleActionReader.getName());
		simpleAction.setEnclosingWorkflow(simpleActionReader.getEnclosingWorkflow().getName());
		simpleAction.setRole(simpleActionReader.getRole());
		simpleAction.setIsAutomaticallyInstantiated(simpleActionReader.isAutomaticallyInstantiated());
		
		List<String> nextActions = simpleAction.getNextAction(); 
		Set<ActionReader> nextActionReaders = simpleActionReader.getPossibleNextActions();
		
		for (ActionReader actionReader : nextActionReaders)
			nextActions.add(actionReader.getName());
		
		return simpleAction;
	}
	
	private ProcessActionType getProcessActionType(ProcessActionReader processActionReader) {
		ProcessActionType processAction = new ProcessActionType();
		processAction.setName(processActionReader.getName());
		processAction.setEnclosingWorkflow(processActionReader.getEnclosingWorkflow().getName());
		processAction.setRole(processActionReader.getRole());
		processAction.setIsAutomaticallyInstantiated(processActionReader.isAutomaticallyInstantiated());
		processAction.setActionWorkflow(processActionReader.getActionWorkflow().getName());

		return processAction;
	}
	
	private ProcessType getProcessType(ProcessReader processReader) {
		ProcessType process = new ProcessType();
		process.setStartTime(Resources.getXMLGregorianCalendar((GregorianCalendar) processReader.getStartTime()));
		process.setWorkflow(processReader.getWorkflow().getName());
		
		List<ActionStatusType> actionStatusTypes = process.getActionStatus();
		List<ActionStatusReader> actionStatusReaders = processReader.getStatus();
		
		for (ActionStatusReader actionStatusReader : actionStatusReaders)
			actionStatusTypes.add(getActionStatusType(actionStatusReader));
		
		String processId = getRandomString();
		process.setId(processId);
		workflows.get(process.getWorkflow()).getProcesses().add(processId);
		
		return process;
	}
	
	private ActionStatusType getActionStatusType(ActionStatusReader actionStatusReader) {
		ActionStatusType actionStatus = new ActionStatusType();
		actionStatus.setName(actionStatusReader.getActionName());
		actionStatus.setIsTakenInCharge(actionStatusReader.isTakenInCharge());
		actionStatus.setIsTerminated(actionStatusReader.isTerminated());
		
		if (actionStatus.isIsTakenInCharge()) {
			Actor actor = actionStatusReader.getActor();
			ActorType actorType = new ActorType();
			actorType.setName(actor.getName());
			actorType.setRole(actor.getRole());
			actionStatus.setActor(actorType);			
		}
		
		if (actionStatus.isIsTerminated())
			actionStatus.setTerminationTime(Resources.getXMLGregorianCalendar((GregorianCalendar) actionStatusReader.getTerminationTime()));
		
		return actionStatus;		
	}
}
