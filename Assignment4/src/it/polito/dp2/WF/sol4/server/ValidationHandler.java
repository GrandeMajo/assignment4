package it.polito.dp2.WF.sol4.server;

import java.io.File;
import java.util.Set;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.Detail;
import javax.xml.soap.DetailEntry;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.xml.sax.SAXException;

import it.polito.dp2.WF.lab4.gen.server.InvalidRequestError;
import it.polito.dp2.WF.lab4.gen.server.InvalidRequestError_Exception;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;


public class ValidationHandler implements SOAPHandler<SOAPMessageContext> {
	
	private static final String SCHEMA_LOCATION = "wsdl/Workflow_schema.xsd";
	private static final String CONTEXT_PATH 	= "it.polito.dp2.WF.lab4.gen.server";
	
	private static Logger logger = Logger.getLogger(ValidationHandler.class.getName());
	
	private JAXBContext jaxbContext;

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		Boolean isOutbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		
		if (isOutbound != null && isOutbound)
			return true;
		
		// inbound message
		SOAPMessage message = context.getMessage();
		
		if (message == null)
			return false;
		
		try {
			SOAPBody body = message.getSOAPBody();
			
			if (body == null) {
				InvalidRequestError error = ErrorUtils.getInvalidRequestError(ErrorUtils.EMPTY_MESSAGE_BODY);
				throw new InvalidRequestError_Exception(ErrorUtils.EMPTY_MESSAGE_BODY, error);
			}
			
            SchemaFactory schemaFactory = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(new File(SCHEMA_LOCATION));

            jaxbContext = JAXBContext.newInstance(CONTEXT_PATH);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setSchema(schema);
            unmarshaller.unmarshal(body.getFirstChild());
			
		} catch (SAXException se) {
			logger.severe(se.getMessage());
			generateSOAPFault(message, ErrorUtils.INTERNAL_SERVER_ERROR);
			return false;
		} catch (SOAPException se) {
			logger.severe(se.getMessage());
			generateSOAPFault(message, ErrorUtils.INTERNAL_SERVER_ERROR);
			return false;
		} catch (UnmarshalException ue) {
			logger.severe(ue.getMessage());
			generateSOAPFault(message, ErrorUtils.VALIDATION_ERROR);
			return false;			
		} catch (Exception e) {
			logger.severe(e.getMessage());
			generateSOAPFault(message, ErrorUtils.INTERNAL_SERVER_ERROR);
			return false;			
		}

		return true;
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void close(MessageContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public Set<QName> getHeaders() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void generateSOAPFault(SOAPMessage message, String reason) {
    	try {
    	    SOAPBody body = message.getSOAPBody();
    	    body.removeContents();
    	    SOAPFault fault = body.addFault();
    	    fault.setFaultString(reason);
    	    Detail detail = fault.addDetail();
    	    QName errorName = new QName("http://pad.polito.it/WorkflowManagement", getLocalPart(reason), "ns2");
    	    DetailEntry errorEntry = detail.addDetailEntry(errorName);
    	    errorEntry.addChildElement("code").addTextNode(ErrorUtils.getErrorCode(reason));
    	    errorEntry.addChildElement("message").addTextNode(reason);
    	} catch(SOAPException se) { }
    }

	private String getLocalPart(String errorKind) {
		switch (errorKind) {
			case ErrorUtils.VALIDATION_ERROR: 		return "invalidRequestError";
			case ErrorUtils.INTERNAL_SERVER_ERROR: 	return "systemError";
			default: return "systemError";
		}
	}

}
