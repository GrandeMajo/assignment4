package it.polito.dp2.WF.sol4.server;

import java.util.UUID;
import java.util.logging.Logger;

import javax.jws.HandlerChain;
import javax.jws.WebService;

import it.polito.dp2.WF.lab4.gen.server.InvalidRequestError_Exception;
import it.polito.dp2.WF.lab4.gen.server.ProcessCreator;
import it.polito.dp2.WF.lab4.gen.server.ProcessType;
import it.polito.dp2.WF.lab4.gen.server.SystemError;
import it.polito.dp2.WF.lab4.gen.server.SystemError_Exception;

@WebService(
		name = "ProcessCreator", 
		portName = "ProcessCreator", 
		targetNamespace = "http://pad.polito.it/WorkflowManagement",
		endpointInterface = "it.polito.dp2.WF.lab4.gen.server.ProcessCreator")
@HandlerChain(file = "META-INF/handler-chain.xml")
public class ProcessCreatorImplementation implements ProcessCreator {
	
	private static Logger logger = Logger.getLogger(ProcessCreatorImplementation.class.getName());

	public ProcessCreatorImplementation() {
		super();
	}
	
	public WorkflowManager getWorkflowManagerInstance() throws SystemError_Exception {
		WorkflowManager manager = WorkflowManager.getInstance();
		if (manager == null)
			throw new SystemError_Exception("Error occurred instanziating WorkflowManager", ErrorUtils.getSystemError(ErrorUtils.INTERNAL_SERVER_ERROR));
		
		return manager;
	}
	
	public RequestManager getRequestManagerInstance() throws SystemError_Exception {
		RequestManager manager = RequestManager.getInstance();
		if (manager == null)
			throw new SystemError_Exception("Error occurred instanziating WorkflowManager", ErrorUtils.getSystemError(ErrorUtils.INTERNAL_SERVER_ERROR));
		
		return manager;
	}

	@Override
	public ProcessType createProcess(String workflowName, String clientId, String requestId)
			throws InvalidRequestError_Exception, SystemError_Exception {
		try {	
			RequestManager rManager = getRequestManagerInstance();
			if (rManager.addRequest(UUID.fromString(clientId), requestId)) {
				WorkflowManager wfManager = getWorkflowManagerInstance();
				return wfManager.createProcess(workflowName);					
			}
		} catch(InvalidRequestError_Exception iree) {
			logger.severe(iree.getMessage());
			throw iree;		
		} catch(SystemError_Exception see) {
			logger.severe(see.getMessage());
			throw see;		
		} catch (Exception e) {
			logger.severe(e.getMessage());
			SystemError systemError = ErrorUtils.getSystemError(ErrorUtils.INTERNAL_SERVER_ERROR);
			throw new SystemError_Exception(ErrorUtils.INTERNAL_SERVER_ERROR, systemError);
		}
		return null;
	}

	@Override
	public String getClientId() throws SystemError_Exception {
		try {		
			RequestManager rManager = getRequestManagerInstance();
			return rManager.addClient().toString();
		} catch(SystemError_Exception see) {
			logger.severe(see.getMessage());
			throw see;		
		} catch (Exception e) {
			logger.severe(e.getMessage());
			SystemError systemError = ErrorUtils.getSystemError(ErrorUtils.INTERNAL_SERVER_ERROR);
			throw new SystemError_Exception(ErrorUtils.INTERNAL_SERVER_ERROR, systemError);
		}
	}
}
