package it.polito.dp2.WF.sol4.server;

import java.util.concurrent.Executors;
import java.util.logging.Logger;

import javax.xml.ws.Endpoint;

public class WorkflowServer {
	
	private static Logger logger = Logger.getLogger(WorkflowServer.class.getName());
	
	private static final int POOL_SIZE = 15;
	
	private static final String WORKFLOW_INFO_SERVICE	= "WorkflowInfoService";
	private static final String PROCESS_CREATOR_SERVICE = "ProcessCreatorService";
	private static final String ACTION_MANAGER_SERVICE 	= "ActionManagerService";
	
	private static final String WORKFLOW_INFO_SERVICE_URL	= "http://localhost:7071/wfinfo";
	private static final String PROCESS_CREATOR_SERVICE_URL = "http://localhost:7070/wfcontrol";
	private static final String ACTION_MANAGER_SERVICE_URL 	= "http://localhost:7071/wfaction";
	
	public static void publishEndpoint(String serviceName, String address, int poolSize, Object implementor) {
		System.out.print("*** " + serviceName + " start ***\nPublishing at " + address + " ...");
		Endpoint endpoint = Endpoint.create(implementor);
		endpoint.setExecutor(Executors.newFixedThreadPool(poolSize));
		endpoint.publish(address);
		System.out.println("done.");
	}

	public static void main(String[] args) {
		
		try {
			logger.info(">>>>>>>> STARTING SERVER <<<<<<<<");
			
			publishEndpoint(WORKFLOW_INFO_SERVICE, WORKFLOW_INFO_SERVICE_URL, POOL_SIZE, new WorkflowInfoImplementation());
			publishEndpoint(PROCESS_CREATOR_SERVICE, PROCESS_CREATOR_SERVICE_URL, POOL_SIZE, new ProcessCreatorImplementation());
			publishEndpoint(ACTION_MANAGER_SERVICE, ACTION_MANAGER_SERVICE_URL, POOL_SIZE, new ActionManagerImplementation());
			
			logger.info("WorkflowServer correcly started, endpoints published.");
			
		} catch (Exception e) {
			logger.severe("Unable to star service: " + e.getMessage());
		}

	}

}
