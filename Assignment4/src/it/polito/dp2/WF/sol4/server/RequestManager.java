package it.polito.dp2.WF.sol4.server;

import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import it.polito.dp2.WF.lab4.gen.server.InvalidRequestError;
import it.polito.dp2.WF.lab4.gen.server.InvalidRequestError_Exception;

/**
 * Class that manages clients and requests related data.
 * Implemented using {@code Singleton} pattern.
 * 
 * @author Gianluca Maiorino
 *
 */
public class RequestManager {
	
	private static Logger logger = Logger.getLogger(RequestManager.class.getName());
	
	/*
	 * A ConcurrentHashMap that contains all the client IDs and foreach client register 
	 * all the requests done. 
	 */
	private volatile ConcurrentHashMap<UUID, HashSet<String>> clientIds;
	
	public RequestManager() {
		clientIds = new ConcurrentHashMap<UUID, HashSet<String>>();
	}
	
	public static class RequestManagerHolder {
		private static final RequestManager MANAGER;
		
		static {
			RequestManager temp = null;
			try {
				temp = new RequestManager();
			} catch (Exception e) {
				temp = null;
				logger.severe(e.getMessage());
			}
			MANAGER = temp;
		}
	}
	
	public static RequestManager getInstance() {
		return RequestManagerHolder.MANAGER;
	}
	
	public boolean existClient(String clientId) {
		return clientIds.containsKey(clientId);
	}
	
	public UUID addClient() {
		UUID clientId;
		do {
			// do while cycle to avoid birthday paradox
			clientId = UUID.randomUUID();
		} while (clientIds.putIfAbsent(clientId, new HashSet<String>()) != null);
		
		logger.info("Added Client ID: " + clientId);
		
		return clientId;
	}
	
	public boolean addRequest(UUID clientId, String requestId) throws InvalidRequestError_Exception {
		logger.info("Client " + clientId + " sent request ID " + requestId);
		
		boolean added = false;
		HashSet<String> requestIds = clientIds.get(clientId);
		
		if (requestIds == null) {
			logger.severe("Invalid client ID " + clientId);
			InvalidRequestError error = ErrorUtils.getInvalidRequestError(ErrorUtils.INVALID_CLIENT_ID);
			throw new InvalidRequestError_Exception(ErrorUtils.INVALID_CLIENT_ID, error);
		}
		
		if (!requestIds.contains(requestId)) {
			synchronized (requestIds) {
				added = requestIds.add(requestId);
			}			
		}
		
		if (!added) {
			logger.severe("Invalid request ID " + requestId + " already present");
			InvalidRequestError error = ErrorUtils.getInvalidRequestError(ErrorUtils.INVALID_REQUEST_ID);
			throw new InvalidRequestError_Exception(ErrorUtils.INVALID_REQUEST_ID, error);
		}
		
		return true;
	}

}
