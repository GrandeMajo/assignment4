package it.polito.dp2.WF.sol4.server;

import java.util.List;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.ws.Holder;

import it.polito.dp2.WF.lab4.gen.server.ActionManager;
import it.polito.dp2.WF.lab4.gen.server.ActorType;
import it.polito.dp2.WF.lab4.gen.server.InvalidRequestError_Exception;
import it.polito.dp2.WF.lab4.gen.server.SystemError_Exception;

@WebService(
		name = "ActionManager", 
		portName = "ActionManager",
		targetNamespace = "http://pad.polito.it/WorkflowManagement",
		endpointInterface = "it.polito.dp2.WF.lab4.gen.server.ActionManager")
@HandlerChain(file = "META-INF/handler-chain.xml")
public class ActionManagerImplementation implements ActionManager {
	
	public WorkflowManager getWorkflowManagerInstance() throws SystemError_Exception {
		WorkflowManager manager = WorkflowManager.getInstance();
		if (manager == null)
			throw new SystemError_Exception("Error occurred instanziating WorkflowManager", ErrorUtils.getSystemError(ErrorUtils.INTERNAL_SERVER_ERROR));
		
		return manager;
	}
	
	public RequestManager getRequestManagerInstance() throws SystemError_Exception {
		RequestManager manager = RequestManager.getInstance();
		if (manager == null)
			throw new SystemError_Exception("Error occurred instanziating WorkflowManager", ErrorUtils.getSystemError(ErrorUtils.INTERNAL_SERVER_ERROR));
		
		return manager;
	}

	@Override
	public void takeOverAction(String actionName, String processId, ActorType actor, String clientId, String requestId,
			Holder<Boolean> result, Holder<String> message)
					throws InvalidRequestError_Exception, SystemError_Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void completeAction(String actionName, String processId, ActorType actor, String clientId, String requestId,
			List<String> nextActionName, Holder<Boolean> result, Holder<String> message)
					throws InvalidRequestError_Exception, SystemError_Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getClientId() throws SystemError_Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
