package it.polito.dp2.WF.sol4.server;

import it.polito.dp2.WF.lab4.gen.server.InvalidRequestError;
import it.polito.dp2.WF.lab4.gen.server.SystemError;
import it.polito.dp2.WF.lab4.gen.server.UnknownIdentifiersError;
import it.polito.dp2.WF.lab4.gen.server.UnknownNamesError;

public class ErrorUtils {
	/*
	 * Possible error messages 
	 */
	public static final String UNKNOWN_NAME 			= "Error: workflow name is unknown";
	public static final String UNKNOWN_NAMES 			= "Error: part of the workflow names are unknown";
	public static final String UNKNOWN_IDENTIFIERS 		= "Error: part of the ids are unknown";
	public static final String INVALID_PAGE_NUMBER		= "Error: invalid value for page number, expected a positive integer value";
	public static final String INTERNAL_SERVER_ERROR 	= "Error: internal server error";
	public static final String EMPTY_MESSAGE_BODY	 	= "Error: message body is empty";
	public static final String INITIALIZATION_FAILED 	= "Error: inizialization of data manager failed";
	public static final String INVALID_REQUEST		 	= "Error: unknown error in the request";
	public static final String VALIDATION_ERROR		 	= "Error: error during validation of request";
	public static final String INVALID_CLIENT_ID	 	= "Error: invalid client ID, it is unknown";
	public static final String INVALID_REQUEST_ID	 	= "Error: invalid request ID, already used";
	
	public static String getErrorCode(String errorMessage) {
		switch (errorMessage) {
		case UNKNOWN_NAME:			return "0x0001";
		case UNKNOWN_NAMES:			return "0x0002";
		case UNKNOWN_IDENTIFIERS:	return "0x0003";
		case INVALID_REQUEST:		return "0x0040";
		case VALIDATION_ERROR:		return "0x0041";
		case INVALID_CLIENT_ID:		return "0x0042";
		case INVALID_REQUEST_ID:	return "0x0043";
		case EMPTY_MESSAGE_BODY:	return "0x0044";
		case INTERNAL_SERVER_ERROR:	return "0x00A0";
		case INITIALIZATION_FAILED:	return "0x00A1";

		default: return null;
		}
	}
	
	public static UnknownNamesError getUnknownNamesError() {
		UnknownNamesError error = new UnknownNamesError();
		error.setMessage(UNKNOWN_NAMES);
		error.setCode(getErrorCode(UNKNOWN_NAMES));
		return error;
	}
	
	public static UnknownIdentifiersError getUnknownIdentifiersError() {
		UnknownIdentifiersError error = new UnknownIdentifiersError();
		error.setMessage(UNKNOWN_IDENTIFIERS);
		error.setCode(getErrorCode(UNKNOWN_IDENTIFIERS));
		return error;		
	}

	public static InvalidRequestError getInvalidRequestError(String errorMessage) {
		InvalidRequestError error = new InvalidRequestError();
		error.setMessage(errorMessage);
		error.setCode(getErrorCode(errorMessage));
		return error;
	}
	
	public static SystemError getSystemError(String errorMessage) {
		SystemError error = new SystemError();
		error.setMessage(errorMessage);
		error.setCode(getErrorCode(errorMessage));
		return error;
	}
}
