package it.polito.dp2.WF.sol4.server;

import java.math.BigInteger;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;

import it.polito.dp2.WF.lab4.gen.server.InvalidRequestError_Exception;
import it.polito.dp2.WF.lab4.gen.server.ProcessType;
import it.polito.dp2.WF.lab4.gen.server.SystemError_Exception;
import it.polito.dp2.WF.lab4.gen.server.UnknownIdentifiersError_Exception;
import it.polito.dp2.WF.lab4.gen.server.UnknownNamesError_Exception;
import it.polito.dp2.WF.lab4.gen.server.WorkflowInfo;
import it.polito.dp2.WF.lab4.gen.server.WorkflowType;


@WebService(
		name = "WorkflowInfo", 
		portName = "WorkflowInfo", 
		targetNamespace = "http://pad.polito.it/WorkflowManagement",
		endpointInterface = "it.polito.dp2.WF.lab4.gen.server.WorkflowInfo")
@HandlerChain(file = "META-INF/handler-chain.xml")
public class WorkflowInfoImplementation implements WorkflowInfo {
	
	private static Logger logger = Logger.getLogger(WorkflowInfoImplementation.class.getName());
	
	public WorkflowInfoImplementation() {
		super();
	}
	
	public WorkflowManager getManagerInstance() throws SystemError_Exception {
		WorkflowManager manager = WorkflowManager.getInstance();
		if (manager == null)
			throw new SystemError_Exception("Error occurred instanziating WorkflowManager", ErrorUtils.getSystemError(ErrorUtils.INTERNAL_SERVER_ERROR));
		
		return manager;
	}

	@Override
	public void getWorkflowNames(Holder<XMLGregorianCalendar> lastModifiedTime, Holder<List<String>> workflowNames)
			throws SystemError_Exception {
		logger.entering(WorkflowInfoImplementation.class.getName(), "getWorkflowNames");
		
		try {
			WorkflowManager manager = getManagerInstance();
			workflowNames.value = new LinkedList<String>();								
			GregorianCalendar lmt = (GregorianCalendar) manager.getLastModifiedTime();
			XMLGregorianCalendar calendar = lastModifiedTime.value;
			if (calendar != null) {
				GregorianCalendar lastModTime = calendar.toGregorianCalendar();
				
				if (lastModTime.before(lmt))
					workflowNames.value.addAll(manager.getWorkflowNames());
			} else
				workflowNames.value.addAll(manager.getWorkflowNames());
			
			lastModifiedTime.value = Resources.getXMLGregorianCalendar(lmt);
		} catch (SystemError_Exception se) {
			logger.severe(se.getMessage());
			throw se;
		} catch (Exception e) {
			logger.severe(e.getMessage());
			throw new SystemError_Exception(ErrorUtils.INTERNAL_SERVER_ERROR, ErrorUtils.getSystemError(ErrorUtils.INTERNAL_SERVER_ERROR));
		}

		logger.exiting(WorkflowInfoImplementation.class.getName(), "getWorkflowNames");
	}

	@Override
	public void getWorkflows(List<String> workflowNames, BigInteger pageNumber, Holder<XMLGregorianCalendar> lastModifiedTime, Holder<List<WorkflowType>> workflows, Holder<BigInteger> totalPages)
					throws InvalidRequestError_Exception, SystemError_Exception, UnknownNamesError_Exception {
		try {
			ResultHolder<WorkflowType> result;
			WorkflowManager manager = getManagerInstance();
			int page = (pageNumber == null)? 1 : pageNumber.intValue();

			GregorianCalendar lmt = (GregorianCalendar) manager.getLastModifiedTime();
			XMLGregorianCalendar calendar = lastModifiedTime.value;
			if (calendar != null) {
				GregorianCalendar lastModTime = calendar.toGregorianCalendar();

				if (lastModTime.before(lmt)) {				
					result = manager.getWorkflows(new HashSet<String>(workflowNames), page);
					workflows.value = result.getResults();
					totalPages.value = BigInteger.valueOf(result.getTotalPages());
				} else {
					workflows.value = new LinkedList<WorkflowType>();
					totalPages.value = BigInteger.ZERO;
				}
			} else {
				result = manager.getWorkflows(new HashSet<String>(workflowNames), page);
				workflows.value = result.getResults();
				totalPages.value = BigInteger.valueOf(result.getTotalPages());
			}

			lastModifiedTime.value = Resources.getXMLGregorianCalendar(lmt);
		} catch (UnknownNamesError_Exception une) {
			logger.severe(une.getMessage() + "\nThe unknown names are: " + une.getFaultInfo().getNames());
			throw une;
		} catch (SystemError_Exception se) {
			logger.severe(se.getMessage());
			throw se;
		} catch (Exception e) {
			logger.severe(e.getMessage());
			throw new SystemError_Exception(ErrorUtils.INTERNAL_SERVER_ERROR, ErrorUtils.getSystemError(ErrorUtils.INTERNAL_SERVER_ERROR));
		}
	}

	@Override
	public void getProcesses(List<String> processIds, List<String> workflowNames, BigInteger pageNumber, Holder<XMLGregorianCalendar> lastModifiedTime, Holder<List<ProcessType>> processes, Holder<BigInteger> totalPages) 
					throws InvalidRequestError_Exception, SystemError_Exception, UnknownIdentifiersError_Exception, UnknownNamesError_Exception {
		try {
			ResultHolder<ProcessType> result;
			WorkflowManager manager = getManagerInstance();
			int page = (pageNumber == null)? 1 : pageNumber.intValue();		
			Set<String> pIds = (processIds == null)? null : new HashSet<String>(processIds);
			Set<String> wfNames = (workflowNames == null)? null : new HashSet<String>(workflowNames);
			
			GregorianCalendar lmt = (GregorianCalendar) manager.getLastModifiedTime();
			XMLGregorianCalendar calendar = lastModifiedTime.value;
			if (calendar != null) {
				GregorianCalendar lastModTime = calendar.toGregorianCalendar();
				
				if (lastModTime.before(lmt)) {	
					result = manager.getProcesses(pIds, wfNames, page);
					processes.value = result.getResults();
					totalPages.value = BigInteger.valueOf(result.getTotalPages());
				} else {
					processes.value = new LinkedList<ProcessType>();
					totalPages.value = BigInteger.ZERO;
				}
			} else {
				result = manager.getProcesses(pIds, wfNames, page);
				processes.value = result.getResults();
				totalPages.value = BigInteger.valueOf(result.getTotalPages());
			}
			
			lastModifiedTime.value = Resources.getXMLGregorianCalendar(lmt);			
		} catch (UnknownIdentifiersError_Exception uie) {
			logger.severe(uie.getMessage() + "\nThe unknown IDs are: " + uie.getFaultInfo().getIds());
			throw uie;
		} catch (UnknownNamesError_Exception une) {
			logger.severe(une.getMessage() + "\nThe unknown names are: " + une.getFaultInfo().getNames());
			throw une;
		} catch (SystemError_Exception see) {
			logger.severe(see.getMessage());
			throw see;
		} catch (Exception e) {
			logger.severe(e.getMessage());
			throw new SystemError_Exception(ErrorUtils.INTERNAL_SERVER_ERROR, ErrorUtils.getSystemError(ErrorUtils.INTERNAL_SERVER_ERROR));
		}
	}

	@Override
	public XMLGregorianCalendar getLastModifiedTime() throws SystemError_Exception {
		try {
			WorkflowManager manager = getManagerInstance();
			GregorianCalendar calendar = (GregorianCalendar) manager.getLastModifiedTime();
			return (calendar == null) ? null : Resources.getXMLGregorianCalendar(calendar);			
		} catch (Exception e) {
			logger.severe(e.getMessage());
			throw new SystemError_Exception(ErrorUtils.INTERNAL_SERVER_ERROR, ErrorUtils.getSystemError(ErrorUtils.INTERNAL_SERVER_ERROR));
		}
	}
}
