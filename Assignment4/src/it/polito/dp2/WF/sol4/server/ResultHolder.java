package it.polito.dp2.WF.sol4.server;

import java.util.List;

public class ResultHolder<T> {
	private List<T> results;
	private int totalPages;
	
	public ResultHolder(List<T> results, int totalPages) {
		this.results = results;
		this.totalPages = totalPages;
	}

	public List<T> getResults() {
		return results;
	}

	public void setResults(List<T> results) {
		this.results = results;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
}
