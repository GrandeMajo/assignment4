package it.polito.dp2.WF.sol4.client1;


import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;

import it.polito.dp2.WF.WorkflowMonitorException;
import it.polito.dp2.WF.lab4.gen.client1.InvalidRequestErrorException;
import it.polito.dp2.WF.lab4.gen.client1.ProcessType;
import it.polito.dp2.WF.lab4.gen.client1.SystemErrorException;
import it.polito.dp2.WF.lab4.gen.client1.UnknownIdentifiersErrorException;
import it.polito.dp2.WF.lab4.gen.client1.UnknownNamesErrorException;
import it.polito.dp2.WF.lab4.gen.client1.WorkflowInfo;
import it.polito.dp2.WF.lab4.gen.client1.WorkflowInfoImplementationService;
import it.polito.dp2.WF.lab4.gen.client1.WorkflowType;

public class Client1Thread extends Thread {
	
	private static final Logger logger = Logger.getLogger(Client1Thread.class.getName());
	
	private int times;
	private int sleep;
	private String url;
	private Random random;
	private XMLGregorianCalendar lastUpdate;

	
	public Client1Thread(String url, int times, int sleep) throws WorkflowMonitorException {
		this.url = url;
		this.times = times;
		this.sleep = sleep;
		random = new Random();
	}

	@Override
	public void run() {
		super.run();
		try {
			for (int i = 0; i < times; i++) {
				service();
				Thread.sleep(random.nextInt(sleep));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void service() throws MalformedURLException, SystemErrorException, InvalidRequestErrorException, UnknownNamesErrorException, UnknownIdentifiersErrorException {
		logger.info(">>>>>>>>>>> Starting Client 1 tester... <<<<<<<<<<<");
		WorkflowInfoImplementationService infoService = (url == null) ?
				new WorkflowInfoImplementationService() :
					new WorkflowInfoImplementationService(new URL(url));				
		WorkflowInfo infoProxy = infoService.getWorkflowInfo();
		Holder<XMLGregorianCalendar> lastModifiedTime = new Holder<XMLGregorianCalendar>(lastUpdate);
		Holder<List<String>> workflowNames = new Holder<List<String>>();
		infoProxy.getWorkflowNames(lastModifiedTime, workflowNames);
		if (lastUpdate != null && lastUpdate.equals(lastModifiedTime.value)) {
			logger.info("Info already updated, nothing to request");		
			logger.info(">>>>>>>>>>> Client 1 tester terminated <<<<<<<<<<<");	
			return;
		}
		lastUpdate = lastModifiedTime.value;
		Holder<List<WorkflowType>> workflows = new Holder<List<WorkflowType>>();
		Holder<BigInteger> totalPages = new Holder<BigInteger>(); 
		List<WorkflowType> workflowTypes = new LinkedList<WorkflowType>();
		List<ProcessType> processTypes = new LinkedList<ProcessType>();
		int nPages = 1, i = 1;
		if (!workflowNames.value.isEmpty()) {
			do {
				lastModifiedTime.value = null;	// per ottenere tutti i workflow 
				infoProxy.getWorkflows(workflowNames.value, BigInteger.valueOf(i), lastModifiedTime, workflows, totalPages);
				if (nPages == 1)
					nPages = totalPages.value.intValue();
				workflowTypes.addAll(workflows.value);
			} while (++i <= nPages);
		}
		logger.info("got " + workflowTypes.size() + " workflows: " + workflowTypes);
		List<String> processIds = new LinkedList<String>();
		for (WorkflowType workflowType : workflowTypes)
			processIds.addAll(workflowType.getProcesses());
		logger.info("processes available (" + processIds.size() + "): " + processIds);
		Holder<List<ProcessType>> processes = new Holder<List<ProcessType>>();
		logger.info("Getting processes...");
		nPages = i = 1;
		if (!processIds.isEmpty()) {
			do {
				lastModifiedTime.value = null;	// per ottenere tutti i processi
				infoProxy.getProcesses(processIds, workflowNames.value, BigInteger.valueOf(i), lastModifiedTime, processes, totalPages);
				if (nPages == 1)
					nPages = totalPages.value.intValue();
				processTypes.addAll(processes.value);
			} while (++i <= nPages);				
		}
		logger.info("got " + processTypes.size() + " processes: " + processTypes);
		logger.info(">>>>>>>>>>> Client 1 tester terminated <<<<<<<<<<<");		
	}
}
