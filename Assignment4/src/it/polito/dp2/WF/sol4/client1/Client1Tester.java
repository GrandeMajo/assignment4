package it.polito.dp2.WF.sol4.client1;

import it.polito.dp2.WF.WorkflowMonitorException;

public class Client1Tester {

	public static void main(String[] args) {
		int times 		= 150;
		int sleep 		= 1000;
		int nThreads 	= 2;
		
		try {
			for (int i = 0; i < nThreads; i++)				
				new Client1Thread(args[0], times, sleep).start();
			
		} catch (WorkflowMonitorException e) {
			e.printStackTrace();
		}
	}
}
