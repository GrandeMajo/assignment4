package it.polito.dp2.WF.sol4.client1;

import java.math.BigInteger;
import java.net.URL;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;

import it.polito.dp2.WF.Actor;
import it.polito.dp2.WF.WorkflowMonitorException;
import it.polito.dp2.WF.WorkflowReader;
import it.polito.dp2.WF.lab4.gen.client1.ActionStatusType;
import it.polito.dp2.WF.lab4.gen.client1.ActorType;
import it.polito.dp2.WF.lab4.gen.client1.InvalidRequestErrorException;
import it.polito.dp2.WF.lab4.gen.client1.ProcessActionType;
import it.polito.dp2.WF.lab4.gen.client1.ProcessType;
import it.polito.dp2.WF.lab4.gen.client1.SimpleActionType;
import it.polito.dp2.WF.lab4.gen.client1.SystemErrorException;
import it.polito.dp2.WF.lab4.gen.client1.UnknownIdentifiersErrorException;
import it.polito.dp2.WF.lab4.gen.client1.UnknownNamesErrorException;
import it.polito.dp2.WF.lab4.gen.client1.WorkflowInfo;
import it.polito.dp2.WF.lab4.gen.client1.WorkflowInfoImplementationService;
import it.polito.dp2.WF.lab4.gen.client1.WorkflowType;

public class WorkflowInfoClient {
	private static final int ENCLOSING_WORKFLOW_INDEX	= 0; 
	private static final int ACTION_WORKFLOW_INDEX		= 1; 
	private static final int PROCESS_ACTION_INDEX 		= 2;
		
	private static final int THREADS_NUMBER	= 10;	
	private static final int TIMES_BOUND	= 10;
	private static final int SLEEP_BOUND	= 2000;	// milliseconds
//	private static final int BINDING_PORT	= 8888;
	
	private static final Logger logger = Logger.getLogger(WorkflowInfoClient.class.getName());
	
	private String url;
	private WorkflowMonitorFactory workflowMonitorFactory;
	private WorkflowMonitorImplementation monitor;
	private HashMap<String, ArrayList<String>> unsolvedNextActions;
	private LinkedList<String[]> unsolvedActionWorkflows;
	private XMLGregorianCalendar lastUpdate;

	public WorkflowInfoClient(String url) throws WorkflowMonitorException {
		Resources.setLogHandler(logger, false);
		try {
			this.url = url;
			workflowMonitorFactory = new WorkflowMonitorFactory();
			monitor = (WorkflowMonitorImplementation) workflowMonitorFactory.newWorkflowMonitor();
			unsolvedNextActions = new HashMap<String, ArrayList<String>>(); 
			unsolvedActionWorkflows = new LinkedList<String[]>();
		} catch (WorkflowMonitorException wme) {
			throw wme;
		} catch (Exception e) {
			throw new WorkflowMonitorException(e);
		}
	}

	public WorkflowInfoClient(WorkflowMonitorImplementation monitor, String url) throws WorkflowMonitorException {
		Resources.setLogHandler(logger, false);
		try {
			this.url = url;
			this.monitor = monitor;
			unsolvedNextActions = new HashMap<String, ArrayList<String>>();
			unsolvedActionWorkflows = new LinkedList<String[]>();
		} catch (Exception e) {
			throw new WorkflowMonitorException(e);
		}
	}
	
	public void service() throws WorkflowMonitorException {
		logger.info("Starting Client 1...");
		WorkflowInfoImplementationService infoService;
		try {
			infoService = (url == null) ?
					new WorkflowInfoImplementationService() :
						new WorkflowInfoImplementationService(new URL(url));				
			
			WorkflowInfo infoProxy = infoService.getWorkflowInfo();
//			BindingProvider provider = (BindingProvider) infoProxy;
//			provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://localhost:" + BINDING_PORT + "/wfinfo");
			
			Holder<XMLGregorianCalendar> lastModifiedTime = new Holder<XMLGregorianCalendar>(lastUpdate);
			Holder<List<String>> workflowNames = new Holder<List<String>>();
			infoProxy.getWorkflowNames(lastModifiedTime, workflowNames);
			
			// se non ci sono stati aggiornamenti sul server ritorna
			if (lastUpdate != null && lastUpdate.equals(lastModifiedTime.value)) {
				logger.info("Info already updated, nothing to request");				
				return;
			}
			logger.info("Info out of date, update required");
			logger.info("workflows available (" + workflowNames.value.size() + "): " + workflowNames.value);
			
			lastUpdate = lastModifiedTime.value;
			
			Holder<List<WorkflowType>> workflows = new Holder<List<WorkflowType>>();
			Holder<BigInteger> totalPages = new Holder<BigInteger>(); 
			List<WorkflowType> workflowTypes = new LinkedList<WorkflowType>();
			List<ProcessType> processTypes = new LinkedList<ProcessType>();
			
			logger.info("Getting workflows...");
			
			int nPages = 1, i = 1;
			if (!workflowNames.value.isEmpty()) {
				do {
					lastModifiedTime.value = null;	// per ottenere tutti i workflow 
					infoProxy.getWorkflows(workflowNames.value, BigInteger.valueOf(i), lastModifiedTime, workflows, totalPages);
					if (nPages == 1)
						nPages = totalPages.value.intValue();
					workflowTypes.addAll(workflows.value);
				} while (++i <= nPages);
			}
			
			logger.info("got " + workflowTypes.size() + " workflows: " + workflowTypes);
			
			List<String> processIds = new LinkedList<String>();
			for (WorkflowType workflowType : workflowTypes)
				processIds.addAll(workflowType.getProcesses());
			
			logger.info("processes available (" + processIds.size() + "): " + processIds);
			
			Holder<List<ProcessType>> processes = new Holder<List<ProcessType>>();
			
			logger.info("Getting processes...");
			
			nPages = i = 1;
			if (!processIds.isEmpty()) {
				do {
					lastModifiedTime.value = null;	// per ottenere tutti i processi
					infoProxy.getProcesses(processIds, workflowNames.value, BigInteger.valueOf(i), lastModifiedTime, processes, totalPages);
					if (nPages == 1)
						nPages = totalPages.value.intValue();
					processTypes.addAll(processes.value);
				} while (++i <= nPages);				
			}
			
			logger.info("got " + processTypes.size() + " processes: " + processTypes);
			
			for (WorkflowType workflowType : workflowTypes)
				monitor.addWorkflow(getWorkflow(workflowType));
			
			solveActionWorkflows();
			
			for (ProcessType processType : processTypes)
				monitor.addProcess(getProcess(processType));
				
		} catch (UnknownIdentifiersErrorException uie) {
			logger.severe(uie.getMessage() + "\nThe unknown IDs are: " + uie.getFaultInfo().getIds());
			throw new WorkflowMonitorException(uie);
		} catch (UnknownNamesErrorException une) {
			logger.severe(une.getMessage() + "\nThe unknown names are: " + une.getFaultInfo().getNames());
			throw new WorkflowMonitorException(une);
		} catch (InvalidRequestErrorException iree) {
			logger.severe(iree.getMessage());
			throw new WorkflowMonitorException(iree);
		} catch (SystemErrorException see) {
			logger.severe(see.getMessage());
			throw new WorkflowMonitorException(see);
		} catch (Exception e) {
			logger.severe(e.getMessage());
			throw new WorkflowMonitorException(e);
		} finally {
			logger.info("Client 1 terminated");
		}
	}
	
	public Workflow getWorkflow(WorkflowType workflowType) throws WorkflowMonitorException {
		String name = workflowType.getName();
		if (name == null || name.isEmpty())
			throw new WorkflowMonitorException("Workflow name attribute is null or empty.");
		if (!name.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid workflow name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		
		Workflow workflow = new Workflow(name);
		
		List<SimpleActionType> simpleActionTypes = workflowType.getSimpleAction();
		for (SimpleActionType simpleActionType : simpleActionTypes)
			workflow.addAction(getSimpleAction(simpleActionType, workflow));
		
		List<ProcessActionType> processActionTypes = workflowType.getProcessAction();
		for (ProcessActionType processActionType : processActionTypes)
			workflow.addAction(getProcessAction(processActionType, workflow));
		
		for (Entry<String, ArrayList<String>> entry : unsolvedNextActions.entrySet()) {
	    	SimpleAction action = (SimpleAction) workflow.getAction(entry.getKey());
	    	ArrayList<String> nextActions = entry.getValue();
	    	for (String nextAction : nextActions) {
	    		if (!nextAction.matches(Resources.NAME_REGEX))
	    			throw new WorkflowMonitorException("Invalid nextAction name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
	    		Action a = (Action) workflow.getAction(nextAction);
	    		if (a == null)
	    			throw new WorkflowMonitorException("Invalid name for next action " + nextAction + ", action not exists.");
	    		
	    		action.addPossibleNextAction(a);
			}
	    }
		unsolvedNextActions.clear();
		
		return workflow;
	}
	
	public Process getProcess(ProcessType processType) throws WorkflowMonitorException {
		String workflowName = processType.getWorkflow();
		if (workflowName == null || workflowName.isEmpty())
			throw new WorkflowMonitorException("Process workflow attribute is null or empty.");
		if (!workflowName.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid workflow name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		
		Workflow workflow = (Workflow) monitor.getWorkflow(workflowName);
		if (workflow == null)
			throw new WorkflowMonitorException("Invalid workflow.");
		
		XMLGregorianCalendar calendar = processType.getStartTime();
		if (calendar == null || !calendar.isValid())
			throw new WorkflowMonitorException("Date time not present or invalid.");
		
		Process process = new Process(calendar.toGregorianCalendar(), workflow);
		
		List<ActionStatusType> actionStatusTypes = processType.getActionStatus();
		for (ActionStatusType actionStatusType : actionStatusTypes) {
			process.addActionStatus(getActionStatus(actionStatusType, process, workflow));
		}
		
		workflow.addProcess(process);
		
		return process;
	}
	
	public SimpleAction getSimpleAction(SimpleActionType simpleActionType, WorkflowReader enclosingWorkflow) throws WorkflowMonitorException {
		String name = simpleActionType.getName();
		if (name == null || name.isEmpty())
			throw new WorkflowMonitorException("Action name attribute is null or empty.");
		if (!name.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid simpleAction name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");

		String role = simpleActionType.getRole();
		if (role == null || role.isEmpty())
			throw new WorkflowMonitorException("Action role attribute is null or empty.");
		if (!role.matches(Resources.ROLE_REGEX))
			throw new WorkflowMonitorException("Invalid role name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");

		String workflow = simpleActionType.getEnclosingWorkflow();
		if (workflow == null || workflow.isEmpty())
			throw new WorkflowMonitorException("Action enclosingWorkflow attribute is null or empty.");
		if (!name.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid action enclosingWorkflow name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		if (!workflow.equals(enclosingWorkflow.getName()))
			throw new WorkflowMonitorException("Invalid enclosingWorkflow.");
		
		SimpleAction simpleAction = new SimpleAction(name, role, enclosingWorkflow, simpleActionType.isIsAutomaticallyInstantiated());
		unsolvedNextActions.put(name, (ArrayList<String>) simpleActionType.getNextAction());
		
		return simpleAction;
	}
	
	public ProcessAction getProcessAction(ProcessActionType processActionType, WorkflowReader enclosingWorkflow) throws WorkflowMonitorException {
		String name = processActionType.getName();
		if (name == null || name.isEmpty())
			throw new WorkflowMonitorException("Action name attribute is null or empty.");
		if (!name.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid processAction name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		
		String role = processActionType.getRole();
		if (role == null || role.isEmpty())
			throw new WorkflowMonitorException("Action role attribute is null or empty.");
		if (!role.matches(Resources.ROLE_REGEX))
			throw new WorkflowMonitorException("Invalid role name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		
		String workflow = processActionType.getEnclosingWorkflow();
		if (workflow == null || workflow.isEmpty())
			throw new WorkflowMonitorException("Action enclosingWorkflow attribute is null or empty.");
		if (!name.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid action enclosingWorkflow name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		if (!workflow.equals(enclosingWorkflow.getName()))
			throw new WorkflowMonitorException("Invalid enclosingWorkflow.");
		
		Boolean ai = processActionType.isIsAutomaticallyInstantiated();
		boolean automaticallyInstantiated = (ai == null)? false : ai;  
		ProcessAction processAction = new ProcessAction(name, role, enclosingWorkflow, automaticallyInstantiated);
		
		String actionWorkflowName = processActionType.getActionWorkflow();
		if (actionWorkflowName == null || actionWorkflowName.isEmpty())
			throw new WorkflowMonitorException("Action actionWorkflowName attribute is null or empty.");
		if (!actionWorkflowName.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid actionWorkflowName name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		
		Workflow actionWorkflow = (Workflow) monitor.getWorkflow(actionWorkflowName);
		if (actionWorkflow != null) {
			processAction.setActionWorkflow(actionWorkflow);
		} else {
			unsolvedActionWorkflows.add(new String[] { enclosingWorkflow.getName(), actionWorkflowName, name });
		}
		
		return processAction;
	}
	
	public ActionStatus getActionStatus(ActionStatusType actionStatusType, Process process, Workflow workflow) throws WorkflowMonitorException {
		String actionName = actionStatusType.getName();
		if (actionName == null || actionName.isEmpty())
			throw new WorkflowMonitorException("ActionStatus name attribute is null or empty.");
		if (!actionName.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid actionStatus name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		Action action = (Action) workflow.getAction(actionName);
		if (action == null)
			throw new WorkflowMonitorException("The corrispondent action in workflow not exists.");
		
		XMLGregorianCalendar calendar = actionStatusType.getTerminationTime();
		GregorianCalendar terminationTime = null;
		if (calendar != null) {
			if (!calendar.isValid())
				throw new WorkflowMonitorException("Date time not present or invalid.");
			
			terminationTime = calendar.toGregorianCalendar();
			if (terminationTime.before(process.getStartTime()))
				throw new WorkflowMonitorException("Invalid termination time.");			
		}
		
		ActorType a = actionStatusType.getActor();
		
		Actor actor = null;
		if (a != null) {
			String actorName = a.getName(), actorRole = a.getRole();
			if (actorName == null || actorRole == null)
				throw new WorkflowMonitorException("Actor name or role not present.");
			if (!actorName.matches(Resources.ROLE_REGEX) || !actorRole.matches(Resources.ROLE_REGEX))
				throw new WorkflowMonitorException("Invalid actor name format or role name format, they must be an alphabetic string.");
			if (!action.getRole().equals(actorRole))
		    	throw new WorkflowMonitorException("Invalid role value, expected " + action.getRole() + ".");

			actor = new Actor(a.getName(), a.getRole());
		}
		
		boolean tc = (actionStatusType.isIsTakenInCharge() == null)? false : actionStatusType.isIsTakenInCharge().booleanValue();
		boolean t  = (actionStatusType.isIsTerminated() == null)? false : actionStatusType.isIsTerminated().booleanValue();
		if (tc == (actor == null))
	    	throw new WorkflowMonitorException("IsTakenInCharge and actor value are wrong.");
	    if (t == (terminationTime == null))
	    	throw new WorkflowMonitorException("IsTerminated and terminationTime value are wrong.");
	    if ((actor == null) && (terminationTime != null))
	    	throw new WorkflowMonitorException("terminationTime value and actor value are wrong.");
		
		return new ActionStatus(actionName, actor, terminationTime);
	}
	
	private void solveActionWorkflows() throws WorkflowMonitorException {
		for (String[] unsolved : unsolvedActionWorkflows) {
			Workflow actionWorkflow = (Workflow) monitor.getWorkflow(unsolved[ACTION_WORKFLOW_INDEX]);			
			if (actionWorkflow == null)
				throw new WorkflowMonitorException("Invalid " + Resources.ACTION_WORKFLOW + " name, it not exists.");
			
			Workflow enclosingWorkflow = (Workflow) monitor.getWorkflow(unsolved[ENCLOSING_WORKFLOW_INDEX]);
			ProcessAction processAction = (ProcessAction) enclosingWorkflow.getAction(unsolved[PROCESS_ACTION_INDEX]);
			processAction.setActionWorkflow(actionWorkflow);
		}
		unsolvedActionWorkflows.clear();
	}
	
	public static long nextLong(Random random, long bound) {
		// error checking and 2^x checking removed for simplicity.
		long bits, val;
		do {
			bits = (random.nextLong() << 1) >>> 1;
			val = bits % bound;
		} while (bits - val + (bound - 1) < 0L);
		return val;
	}
	
	@SuppressWarnings("unused")
	private static void multiclientTester() {
		for(int t = 0; t < THREADS_NUMBER; t++) {
			new Thread(new Runnable() {
				public void run() {
					try {
						WorkflowInfoClient client = new WorkflowInfoClient("http://localhost:7071/wfinfo");
						Random random = new Random();
						int times = random.nextInt(TIMES_BOUND) + 50;

						for (int i = 0; i < times; i++) {				
							client.service();
							Thread.sleep(random.nextInt(SLEEP_BOUND));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}).start();
		}
	}
	
//	public static void main(String[] args) {
//		try {
//			multiclientTester();
////			WorkflowInfoClient client = new WorkflowInfoClient("http://localhost:7071/wfinfo");
////			client.service();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
//	public static void main(String[] args) {
//		for(int t = 0; t < THREADS_NUMBER; t++) {
//			new Thread(new Runnable() {
//
//				public void run() {
//					try {
//						WorkflowInfoClient client = new WorkflowInfoClient("http://localhost:7071/wfinfo");
//						Random random = new Random();
//						int times = random.nextInt(TIMES_BOUND) + 50;
//
//						for (int i = 0; i < times; i++) {				
//							client.service();
//							Thread.sleep(nextLong(random, SLEEP_BOUND));
//						}
//
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//			}).start();
//		}
//	}
}
